### Test Review and Study Guide 2


## Eliminating Bias in a Search Engine Algorithm

# Algorithm/Question:
A confidential algorithm is embedded in the logic of a search engine to show results based on typical content selected by other users in the same age group as the user performing the search. Content that is not typical for the age group of the user has a lower priority and is at the bottom of the list of search results or not shown at all.
Which of the following is the best option for the future of this algorithm?

# What I answered and how it was wrong:
I answered that the algorithm is showing good results and should remain as it is. But I forgot to account for bias one person will have like on a topic like favorite superhero.


## Public Key Encryption and Symmetric Key Encryption

# Question:
Which of the following statements is most accurate concerning encryption?

# What I answered and how it was wrong:
In this question I thought a public key used the same key for encryption and decryption but insted uses a public key for encrytion and a private key for decryption and same for the opposite for symetric encryption.


## Game Show

# Procedure and Question:
Assume there is a procedure called rollDie(), which correctly returns a random integer between 1 and 6
inclusively and all values are equally likely.
A game is played where a contestant rolls a pair of dice. If both dice result in 1
, then the contestant wins 25 dollars. If one of the dice results in 1, then the contestant rolls the pair of dice again. In this case, the player wins the sum of the dice in dollars, for example, 4 and 3 result in 7
dollars. In all other cases, the contestant wins no money.
A procedure called playGame is shown below:

PROCEDURE playGame()
{
       first ← rollDie()
       second ← rollDie()

       IF(first = 1   AND    second = 1)
       {
                RETURN("25")
       }

       IF(first = 1    OR    second = 1)
       {
                RETURN(first + second)
       }

       RETURN("0")

}

However, the procedure has an error. Which of the following is the best description of the error?

# What I answered and how it was wrong:
I answered the IF statements should be swapped. Thats wrong because there is not an if else statement and it is returining a value so it wouldn't need to be switched. And what would need to happen is we would have to reroll the di in the code because line 45 should be moved down two lines and lines 45 and 45 should be rerolling the dice so those lines shoud be the same as line 35 and 36.
