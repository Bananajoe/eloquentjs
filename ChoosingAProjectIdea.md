
Raw
Blame
History
# Project Brainstorm


## Idea 1:

### Purpose
A quiz to determine if you are happy

### Ways to narrow it down
Explain how the code works by having a number system to decide if you are happy or not.

### List Possibilities
1. A list of questions that will be picked randomly.
2. A list that will grab a phrase at the end depending on what the outcome to make the users day better.

### Function Opportunity's 
1. A funtion when everytime a question is answered it either gives a point or no point depending on the answer to determine the outcome.
2. A function that will randomly grab a question fo the user to answer.

Do I already have the skills to do this: Yes


## Idea 2:
 
### Purpose 
A Raffle choosing machine
 
### Ways to narrow it down
So first the code will get a list of names choose one st random then get a prize from a list of prizes
 
### List Possibilities
1. A list of Prizes that will be picked randomly.
2. A list that will grab a name at random.

### Function Opportunity's 
1. A funtion when everytime a prize is chosen in randomly chooses a name.
2. A funtion when everytime a prize is chosen in randomly chooses a prize.

Do I already have the skills to do this: Yes
