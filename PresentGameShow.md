# Game Show

## Procedure:
 Assume there is a procedure called rollDie(), which correctly returns a random integer between 1 and 6 inclusively and all values are equally likely.
A game is played where a contestant rolls a pair of dice. If both dice result in 1, then the contestant wins 25 dollars. If one of the dice results in 1, then the contestant rolls the pair of dice again. In this case, the player wins the sum of     the dice in dollars, for example, 4 and 3 result in 7 dollars. In all other cases, the contestant wins no money.

A procedure called playGame is shown below:

       PROCEDURE playGame()
      {
        first ← rollDie()
       second ← rollDie()

       IF(first = 1   AND    second = 1)
        {
                 RETURN("25")
        }

        IF(first = 1    OR    second = 1)
        {
                 RETURN(first + second)
        }

       RETURN("0")

        }

## Question:
However, the procedure has an error. Which of the foll    owing is the best description of the error?

## Posible Answers:

### A
If the first condition is ever true and both dice are 1, the second condition will be true and will return the sum of the dice instead of 25.

### B
The RETURN("0") should be attached to an ELSE statement.

### C
The order of the IF statements should be swapped. 

### D
Two more calls to rollDie() need to be made in the second IF statement.

### Why A is wrong:
The reason why A is wrong is because the first condition is the one that will result in 25 and wont continue in the code because of the return statement.

### Why B is wrong:
It wouldn't matter if the return ("0") is in an if statement becuse it's at the end of the code and will return if none of the conditions are met.

### Why I answered C and why it's wrong:
I answered C because I thought the addition IF STATEMENT should have gone first because it isn't just giving you a straight answer like the first IF STATEMENT but that wouldn't fix the bug.

### Why D is right:
The correct answer is D because in the second if statement it didn't follow the conditions that the procedure gave us which once one roll is 1 u have to reroll the dice and you will get the sum in money rather than the original being added together.

#### Fixed code prtion of the second if satement in pseuodo :
    IF(first = 1 OR second = 1)
    {
        first <- rollDie
        second <- rollDie

        RETURN(first + second)
    }   

### Type of Question
I think this is a Developing Procedures Question.
