const readline = require('readline').createInterface({
	input: process.stdin,	output: process.stdout
})

function ask(prompt) {
return new Promise((resolve, reject) => {
    readline.question(prompt, input => resolve(input))
})
}

function randomNumber(max) {
	return Math.floor(Math.random() * max)
}


const mList = ["tacos", "buritos", "tamales", "quesadillas", "chilaquiles", "enchiladas", "fajitas","carne asada", "Tostada", "paellas"]

const cList = ["dumplings", "noodles", "kung pow chicken", "hot pot", "tofu", "steamed buns", "black fungus", "Chinese crepes", "fried rice", "Peking duck"]

const aList = ["steak", "hamburgers", "hot dogs", "cornbread", "corn on the cob", "apple pie", "cheesecake", "pizza", "barbecue", "fried chicken"]

const iList = ["pizza", "caprese salad", "lasagna", "ravioli", "tortellini", "tiramisu", "gelato", "a cappucino", "spaghetti", "risotto"]

question()

async function question(){
let foodType = await ask ("What type of food would you like to eat?(Mexican, Chinese, American, Italian) ")
switch (foodType.toLowerCase()){
	case "mexican":
		console.log("Try " + mList[randomNumber(mList.length)] + ".")
		break
	case "chinese":
		console.log("Try " + cList[randomNumber(cList.length)] + ".")
		break
	case "american":
		console.log("Try " + aList[randomNumber(aList.length)] + ".")
		break
	case "italian":
		console.log("Try " + iList[randomNumber(iList.length)] + ".")
		break
}
end()
}

async function end(){
let endQ = await ask ("Do you want another recomendation?(Y, N) ")
switch (endQ.toLowerCase()){
    case "y":
        question()
        break
    case "n":
        process.exit()
}       
}
