# Test Review and Study Guide
## Cash Register
### Question:
This question was asking which code segment would calculate if a customer could pay what they owed including tax. 
### How I answered and how I fixed it:
On this question I chose an aswer that had a >= rather than > which would make the exact amount you need to pay, not enough money which would end up giving the wrong answer when running the code.
## App Collaboration
### Question:
What of the following is the best example of using collaboration to avoid bias in the development of an aplliaction?
### How I answered and how I fixed it:
When I answered this question I chose pair programing which  wouldn't avoid bias completely because its not a great enough group so it was go interview students from different schools so you can get more opinions which will aviod bias.
## Procedure Error
### Question:
The question gave us a program that should of found the biggest number that is even?
### How I answered and how I fixed it:
So I was being dumb and didn't read the title of the quetion and thought the code had no bugs but it did and rather than giving you the greatest even number 8 my answer, it gave you the biggest number of all 13 which was the correct answer.
## Non-Fault Tolerance
### Question: 
If the internet was not fault-tolerant, which of the following would be a direct consequence?
### How I answered and how I fixed it:
So when I was reading the wuestion I miss interpreted the question but I chose the most least likely answer where people would have to rely on our phones but the true answer is that the internet would shut down for a large group of people which is correct because there wouldn't be another server to connect to.
## Lossy and Lossless Text Compression Algorithms
### Question: 
The question is showing a lossy and a lossless algorithm and its asking what is true about these algorithms
### How I answered and how I fixed it:
I chose the correct identifications for the algorithms but what I got wrong was, that I thought that lossless would shorten the number of packets that lossy would but that wrong because while lossy just removes some of the packets being sent, while lossless has to send an algorithm to decrypte the file.
