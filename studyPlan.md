# Trostin's Study Plan

## How I did on Each Big Idea

Big idea 1, Creative development: 3/8 or 38% wrong

Big idea 2, Data: 6/18 or 38% wrong

Big idea 3, Algorithms and Programmning: 11/24 or 46% wrong

Big idea 4, Computer Systems and Networks: 1/7 or 14% wrong

Big idea 5, Impact of Computing: 2/15 or 13% wrong

## Which Ideas I Plan to Study?

I plan to study big idea 2 and 3 because those are the ones I got the most wrong in.

## What Recourses do I plan to Use and How?

First I would like to review what I got wrong on the test.

### Khan Academy:
To study I plan to mainly use Khan Academy to look at topics that I got wrong. This will let me understand and learn new things.

### Wikepedia:
And I will use wikepedia to reaserch definitions and learn origins of certain topics.
